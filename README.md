# Como solucionar el error No se pudo bloquear var_lib_dpkg_lock - open  Recurso no disponible temporalmente

- El primer paso es matar el proceso de actualización pendiente.

    ```bash
    sudo fuser -vki /var/lib/dpkg/lock
    ```
En la comanda utilizamos el parámetro *-v* para ver las acciones que ejecuta, el parámetro *-k* para matar los procesos de actualización pendientes y el por último el paràmetro -i para ver que programas matara y pida permiso para ello.

- A continuación podemos elimiar el fichero con los datos de la actualización que salio mal.

    ```bash
    sudo rm -f /var/lib/dpkg/lock
    ```
Con esto ya estaría listo pero es recomendables depues de cada actualización ejecutar el siguiente comando y antes de iniciar una para eliminar y reparar paquetes rotos.

    ```bash
    sudo apt autoremove
    ```